Cloning of the execution of the Bash top command where you can check how the different values of CPU or memory of a Linux machine are obtained.
It is fully functional but its construction has been oriented to an educational environment.

The execution can be done by:

$ top.sh

$ top.sh -Irix

Sorry about the comments. They are in Spanish