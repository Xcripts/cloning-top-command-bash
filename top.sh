#!/bin/bash

# Parámetros de la aplicación. Modo Irix
IRIX="$1"

# Declaración de variables
CPU_USAGE=0

# Comprobamos si bash soporta arrays
whotest[0]='test' || (echo 'Ups, something is not right. This version of bash is not compatible' && exit 2)

# Limpia pantalla
clear

# Listado de procesos del directorio /proc
PIDs=$(ls -l /proc/ | awk '$9 ~ /^[0-9]/ {print $9}')

# Bucle para recorrer todos los procesos previamente listados. Itera dos veces con 1 segundo
# en cada iteración. Los tiempos finales se almacenan en arrays
for i in 1 2
do
	while read -r line
	do
		# Comprobar existencia de "stat" en proceso
		if [ -f "/proc/$line/stat" ]
		then
			# Obtener tiempos de usuario y nucleo del proceso y almacenarlos
			TMP_USER_TIMES=$(cat "/proc/$line/stat" | awk '{print $14}')
			TMP_SYS_TIMES=$(cat "/proc/$line/stat" | awk '{print $15}')

			# Calcular tiempos del proceso y tiempos de CPU, y almacenarlos en
			# una variable junto con el PID del proceso
			if [ "$i" -eq 1 ]
			then
				TOTAL_TIMES1+=("$line - $(($TMP_USER_TIMES+$TMP_SYS_TIMES))")
			elif [ "$i" -eq 2 ]
			then
				TOTAL_TIMES2+=("$line - $(($TMP_USER_TIMES+$TMP_SYS_TIMES))")
			fi
		fi
	done <<< "$PIDs"

	# Esperar un segundo y obtener los tiempos de CPU mediante la suma de valores
	# de /proc/stat
	if [ "$i" -eq 1 ]
	then
		PROC_TIME1=$(head -n 1 "/proc/stat" | awk '{print $2+$4+$5+$6+$7+$8}')
		sleep 1s
	else
		PROC_TIME2=$(head -n 1 "/proc/stat" | awk '{print $2+$4+$5+$6+$7+$8}')
	fi
done

# Diferencia de tiempos de CPU obtenidos previamente
PROC_TIME=$(bc <<< "$PROC_TIME2-$PROC_TIME1")

# Comprobación de concordancia entre los arrays generados
if [ "${#TOTAL_TIMES1[@]}" -ne "${#TOTAL_TIMES2[@]}" ]
then
	echo 'Ups, something is not right. There are a problem with the arrays' && exit 2
fi

# Bucle para calcular la diferencia de tiempos final. Con los tiempos obtenidos en el paso
# anterior, se recorren y se obtiene un nuevo array con los datos finales
for i in $(seq 0 $(bc <<< "${#TOTAL_TIMES1[@]}-1"))
do
	# Comprobación de PID equivalente antes de realizar operaciones
	if [ $(echo "${TOTAL_TIMES1[$i]}" | awk '{print $1}') -eq $(echo "${TOTAL_TIMES2[$i]}" | awk '{print $1}') ]
	then
		# Cálculo de diferencia de tiempos
		TOTAL_TIMES=$(bc  <<< "($(echo ${TOTAL_TIMES2[$i]} | awk '{print $3}') - $(echo ${TOTAL_TIMES1[$i]} | awk '{print $3}'))")

		# Cálculo de porcentajes totales de utilizacion por proceso
		TMP_TOTAL_PCT=$(bc <<< "scale=3; ($TOTAL_TIMES/$PROC_TIME)*100")

		TOTAL_PCT_PRO+=("$TMP_TOTAL_PCT - $(echo $(echo ${TOTAL_TIMES1[$i]} | awk '{print $1}'))")

		# Cálculo de porcentaje total de CPU en base 100
		CPU_USAGE=$(bc <<< "scale=3; $CPU_USAGE+$TMP_TOTAL_PCT")
	fi
done

# Cálculo de la memoria total, libre y utilizada
TOTAL_MEM=$(head -n 1 "/proc/meminfo" | awk '{print $2}')
FREE_MEM=$(cat "/proc/meminfo" | grep 'MemFree' | awk '{print $2}')

BUFF_MEM=$(cat "/proc/meminfo" | grep 'Buffers' | awk '{print $2}')
CACH_MEM=$(cat "/proc/meminfo" | grep 'Cached' | awk '{print $2}' | head -n 1)
SWAP_MEM=$(cat "/proc/meminfo" | grep 'SwapCached' | awk '{print $2}')

USE_MEM=$(bc <<< "$TOTAL_MEM - ($BUFF_MEM + $CACH_MEM + $FREE_MEM + $SWAP_MEM)")

# Ordenar procesos por CPU en uso de forma decreciente
IFS=$'\n' TOTAL_PCT_PRO=($(sort -nr <<< "(${TOTAL_PCT_PRO[*]}"))
unset IFS

# Cabecera de la aplicación
echo -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
echo -n "Process number: "
echo "${#TOTAL_PCT_PRO[@]}"
echo -n "CPU usage: "
echo "$CPU_USAGE %"
echo -n "Total Memory: "
echo "$TOTAL_MEM kB"
echo -n "Usage Memory: "
echo "$USE_MEM kB"
echo -n "Free Memory: "
echo "$FREE_MEM kB"
echo -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Información relativa a los procesos
# Cabecera
echo
echo "---------------------------------------------------------------------------------------"
echo "|  PID |  USER  |  PR  |    VIRT    | S |  %CPU  |  %MEM  |    TIME    |   COMMAND    |"
echo "---------------------------------------------------------------------------------------"
# Procesos
for i in $(seq 0 9)
do
	TMP_PROC=("$(echo ${TOTAL_PCT_PRO[$i]} | awk '{print $3}')")

	# PID del proceso
	PID=("$TMP_PROC")

	# Usuario que invoca el proceso
	USER=("$(id -nu $(cat /proc/$TMP_PROC/status | grep 'Uid' | awk '{print $2}'))")

	# Prioridad
	PR=("$(cat /proc/$TMP_PROC/stat | awk '{print $18}')")

	if [ "$PR" -eq -100 ]
	then
		PR="rt"
	fi

	# Tamaño de la memoria virtual
	VIRT=("$(cat /proc/$TMP_PROC/stat | awk '{print $23}')")

	# Estado del proceso
	S=("$(cat /proc/$TMP_PROC/stat | awk '{print $3}')")

	# Porcentaje de uso del procesador en modo Irix seleccionado por el usuario, o
	# modo Solaris por defecto
	if [ "$IRIX" = "-irix" ]
	then
		CPU=$(bc <<< "($(echo ${TOTAL_PCT_PRO[$i]} | awk '{print $1}')* $(nproc))")

		# Comprobación de CPU mayor de 100%
		if [ $(echo " $CPU > 100" | bc) -eq 1 ]
		then
			CPU=100.000
		fi
	else
		CPU=("$(echo ${TOTAL_PCT_PRO[$i]} | awk '{print $1}')")
	fi

	if [[ "$CPU" =~ ^"." ]]
        then
               CPU=("0$CPU")
        fi

	# Porcentaje de uso de memoria
	TMP_MEM_TOT=("$(head -n 1 /proc/meminfo | awk '{print $2}')")

	TMP_MEM_PROC=("(($(cat /proc/$TMP_PROC/stat | awk '{print $24}') * ($(getconf PAGE_SIZE) / 1024)) / $TMP_MEM_TOT) * 100")

	MEM=$(echo -n $(bc <<< "scale=3; $TMP_MEM_PROC"))

	if [[ "$MEM" =~ ^"." ]]
        then
                MEM=("0$MEM")
        fi

	# Tiempo de ejecución del proceso
	TMP_TIME=("$(cat /proc/$TMP_PROC/stat | awk '{print $22}')")

	if [ -z "$TMP_TIME" ]
	then
		TMP_TIME=0
	fi

	# Cálculo de tiempos
	UPTIME=("$(cat /proc/uptime | awk '{print $1}')")
	SECONDS=$(bc <<< "${UPTIME%.*} - ($TMP_TIME/$(getconf CLK_TCK))")
	TIME=$(printf '%dh:%dm:%ds' $(bc <<< "$SECONDS/3600") $(bc <<< "$SECONDS%3600/60") $(bc <<< "$SECONDS%60"))

	# Nombre del programa invocado
	COM=("$(head -n 1 /proc/$TMP_PROC/status | awk '{print $2}')")

# Formato e impresión de los datos
FORMAT=" %5d %9s %4s %13d %2s %9s %9s %12s %15s\n"
printf "$FORMAT" "$PID" "$USER" "$PR" "$VIRT" "$S" "$CPU" "$MEM" "$TIME" "$COM"

done

echo "----------------------------------------------------------------------------------------"